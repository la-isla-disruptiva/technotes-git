# e-wtfpl

(ɔ) 2023 Jérémy Viau-Trudel. All Wrongs Reserved.
version: 0.1

## Authors are individuals that collectively work for the Common Good of Humankind

  - Authors of this work dedicate it to the Common Good of Humankind.
  - Authors encorage anyone that could distribute their work, use it, change it or enhance it to do what the fuck they want to.
  - Authors are individuals and do not gives their rights upon their creation to anyone else or any organization.
  - Authors want to share their work and engage themself on a creative conversation.
  - Authors consider that enforcing privacy of their art works is a wast of time and is detrimental to the Humandkind.

## Users are responsible for their usage

  - Any misfortune caused by the use of this work is not intentional.
  - Users are any person or organization that do something with this work.
  - Users are responsible for the consequenses of their actions even when it imply the use of this work.
  - Authors of this work should not be held responsible of any harmful event only because they wrote those lines.

## Not respecting author's interests is unethical

  - Authors of this work encourage anyone that use or modify their work to declare their "interest scope".
  - Any use of this work that do not respect any previous declarations in the "interest scope" chain is considered unethical.

## Any unethical behavior should not use this work

  - Any use of this work may be found unethical for any other reason.
  - Authors automatically delegate to anyone their right to stop unethical use of this work.

## Otherwise

  - Do what the fuck you want to.
