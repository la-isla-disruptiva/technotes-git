# Fusionne plusieurs répos

*Il est parfois nécessaire de combiner 2 repos qui n'ont pas la même origine. Comment faire?*

## Procédure 1: démarre un nouveau repo et ajoute les 2 codes sources

Soit deux répos git
  - `repo_1` sur `url_repo_1`
  - `repo_2` sur `url_repo_2`

```
# Crée un nouveau répo
git init repo && cd repo

# Crée les références aux remotes
git remote add $repo_1 $url_repo_1
git remote add $repo_2 $url_repo_2

# Crée un commit initial vide
git commit -m "Initialise le projet de fusion de code" --allow-empty

# Ajoute le contenu de la branch main de repo_1 après le contenu de main
git rebase --onto main --root ${repo_1}/main 

# ici, on est dans une branche détachée. Alors, on va se créé une branche 'fusion'
git switch -c fusion

# Incorpore le code de repo_2
git rebase --onto fusion --root ${repo_2}/main 

# Crée une branche pour ce nouvel historique
git switch -c fusion2

# Incorpore ces modifications dans la branche main
git checkout main && git merge fusion2

# Nettoie les branches
git branch -D {fusion,fusion2}

```


